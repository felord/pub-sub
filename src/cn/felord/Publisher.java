package cn.felord;

/**
 * The interface Publisher.
 *
 * @param <E> the type parameter
 * @author felord.cn
 * @since 10 :21
 */
public interface Publisher<E> {


    /**
     * Publish.
     *  @param topic the topic
     * @param event the event
     */
    void publish(String topic, E event);
}
