package cn.felord;

/**
 * The interface Subscriber.
 *
 * @author a
 * @since 10 :37
 */
public interface Subscriber  {

    /**
     * On event.
     *
     * @param event the event
     */
    void onEvent(String event);

}
