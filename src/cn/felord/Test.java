package cn.felord;

/**
 * @author a
 * @since 14:31
 **/
public class Test {

    public static void main(String[] args) {


        Publisher<String> objectPublisher = (topic, event) -> Broker.getInstance().broadcasting(topic, event);
        Subscriber weboEvent = event -> System.out.println("webo event");

        Broker.getInstance().addSub("test", event -> System.out.println("wechat event"));
        Broker.getInstance().addSub("test", weboEvent);


        objectPublisher.publish("test", "test event");

        Broker.getInstance().removeSub("test", weboEvent);
        objectPublisher.publish("test", "test event 2");

    }
}
